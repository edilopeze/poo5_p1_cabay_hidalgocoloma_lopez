
package Usuarios;
import Usuarios.Usuario;
import Archivos.ManejoArchivo;
import java.util.Scanner;

import java.util.ArrayList;

public class Asistente extends Usuario{
    
    public Asistente(String nombre, String apellido, String usuario, String contraseña, String tipo){
        super(nombre, apellido, usuario, contraseña, tipo);
    }
    
    public static int menuAs(){
        Scanner sc = new Scanner(System.in);
        System.out.println("\tMENU ASISTENTE");
        System.out.println("1.  Llenar perfil paciente");
        System.out.println("2. Cancelar cita");
        System.out.println("3. Cancelar usuario");
        System.out.println("4. Consultar pacientes con cita");
        int opcion=0;
        while(opcion<1||opcion>4){
            System.out.print("Elija una opción:");
            opcion=sc.nextInt();
        }  
        return opcion;
    }
    
    /**
     * Metodo que recibe como argumento usuario y llena un archivo con el perfil de dicho usuario 
     * @author Edison López
     */
    public void llenarPerfilPaciente(String usuario) {
        Scanner sc = new Scanner(System.in);
        ArrayList<String> archivo = ManejoArchivo.LeeFichero("usuarios.txt");
        String cedula = "";
        int edad = 0;
        double peso = 0;
        double estatura = 0;
        String fechaNac = "";
        char genero = ' ';
        boolean existeUsuario = false;
        for (int i = 1; i < archivo.size(); i++) {
            String[] linea = archivo.get(i).split(";");
            if (usuario.equals(linea[2])) {
                System.out.println("Actualice los datos para " + linea[0] + " " + linea[1]);
                System.out.println("");
                System.out.print("Cédula: ");
                cedula = sc.nextLine();
                System.out.print("Edad: ");
                edad = sc.nextInt();
                System.out.print("Peso: ");
                peso = sc.nextDouble();
                System.out.print("Estatura: ");
                estatura = sc.nextDouble();
                sc.nextLine();
                System.out.print("Fecha de nacimiento: ");
                fechaNac = sc.nextLine();
                System.out.print("Ingrese el género: ");
                genero = sc.next().charAt(0);
                ManejoArchivo.EscribirArchivo("perfilPaciente.txt",usuario+','+cedula+','+String.valueOf(edad)+','+String.valueOf(peso)+','+String.valueOf(estatura)+','+fechaNac+','+String.valueOf(genero));
                existeUsuario = true;
            }
        }
        if (existeUsuario == false) {
            System.out.println("No existe el usuario ingresado");
        }else {
            System.out.println("Se han actualizado con exito los datos...");
        }

    }
    
    public void llenarPacienteCita(ArrayList<Paciente> pacientes){
        if(pacientes.size()>2){
            for(int i=0;i<pacientes.size();i++){
                ArrayList<String> archivo = ManejoArchivo.LeeFichero("usuario.txt");
                for (int j = 1; j < archivo.size(); j++) {
                    String[] linea = archivo.get(j).split(";");
                    ArrayList<String> file = ManejoArchivo.LeeFichero("perfilPaciente.txt");
                    for(int k=1;k<file.size();k++){
                        String[] line = file.get(j).split(";");
                        if(pacientes.get(i).getNombre().equals(linea[0]+linea[1]) && linea[2].equals(line[0])){
                            System.out.println(pacientes.get(i).getNombre()+","+ line[2]+" años");
                        }
                    }     
                }
            }
        }        
    }
    
    public boolean cancelarCita(){
        return true;
    }
    
    public void cancelarUser(){
    }
}
