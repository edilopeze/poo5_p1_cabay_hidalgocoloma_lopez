/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Usuarios;

import Usuarios.Usuario;
import java.util.Scanner;

/**
 *
 * @author ediso
 */
public abstract class Paciente extends Usuario{
    protected String cedula;
    protected int edad;
    protected double peso;
    protected double estatura;
    protected String fechaNac;
    protected char genero;
    protected boolean datosActualizados;

    public Paciente(String nombre, String apellido, String usuario, String contraseña, String tipo, String cedula,int edad,double peso,double estatura,String fechaNac,char genero){
        super(nombre, apellido, usuario, contraseña, tipo);
        this.cedula=cedula;
        this.edad=edad;
        this.peso=peso;
        this.estatura=estatura;
        this.fechaNac=fechaNac;
        this.genero=genero;
        this.datosActualizados=false;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getEstatura() {
        return estatura;
    }

    public void setEstatura(double estatura) {
        this.estatura = estatura;
    }

    public String getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(String fechaNac) {
        this.fechaNac = fechaNac;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public boolean getDatosActualizados() {
        return datosActualizados;
    }

    public void setDatosActualizados(boolean datosActualizados) {
        this.datosActualizados = datosActualizados;
    }
    
    public static int menu(){
        Scanner sc = new Scanner(System.in);
        System.out.println("\tMENU");
        System.out.println("1. Solicitar una cita médica");
        System.out.println("2. Consultar cita pendiente");
        System.out.println("3. Solicitar cancelación de cita");
        int opcion=0;
        while(opcion<1||opcion>4){
            System.out.print("Elija una opción:");
            opcion=sc.nextInt();
        }  
        return opcion;
    }
    
    public static int menuEspecialidades(){
        Scanner sc = new Scanner(System.in);
        System.out.println("\tESPECIALIDADES");
        System.out.println("1. Pediatría");
        System.out.println("2. Ginecología");
        System.out.println("3. Medicina General");
        int opcion=0;
        while(opcion<1||opcion>3){
            System.out.print("Elija una opción:");
            opcion=sc.nextInt();
        }  
        return opcion;
    }
    
    public abstract boolean solicitarCita(String especialidad, Paciente paciente);
    
    public abstract void consultarCita(String codigoCita);
    
    public abstract void cancelarCita(String codCita);
    
    
}
