/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Citas;

/**
 *
 * @author richy
 */
public class Doctor {
    protected String nombre;
    protected String[] horarios;
    protected String especialidad;
    
    public Doctor(String nombre, String[] horarios,String especialidad){
        this.nombre=nombre;
        this.horarios=horarios;
        this.especialidad=especialidad;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String[] getHorarios() {
        return horarios;
    }

    public void setHorarios(String[] horarios) {
        this.horarios = horarios;
    }
    public void mostrarHorarios(){
        for(int i=1;i<this.horarios.length;i++){
            System.out.println(i+". "+this.horarios[0]+", "+this.horarios[i]);
        }
    }
    
}
