/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Citas;
/**
 *
 * @author ediso
 */
public class Cita {
    protected String codCita;
    protected String paciente;
    protected String nombreDoctor;
    protected String fechaConsulta;
    protected String horaConsulta;
    protected String especialidad;

    public Cita(String codCita, String paciente, String nombreDoctor, String fechaConsulta, String horaConsulta, String especialidad) {
        this.codCita = codCita;
        this.paciente = paciente;
        this.nombreDoctor = nombreDoctor;
        this.fechaConsulta = fechaConsulta;
        this.horaConsulta = horaConsulta;
        this.especialidad = especialidad;
    }

    public String getCodigo() {
        return codCita;
    }

    public void setCodigo(String codigo) {
        this.codCita = codigo;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getNombreDoctor() {
        return nombreDoctor;
    }

    public void setNombreDoctor(String nombreDoctor) {
        this.nombreDoctor = nombreDoctor;
    }

    public String getFechaConsulta() {
        return fechaConsulta;
    }

    public void setFechaConsulta(String fechaConsulta) {
        this.fechaConsulta = fechaConsulta;
    }

    public String getHoraConsulta() {
        return horaConsulta;
    }

    public void setHoraConsulta(String horaConsulta) {
        this.horaConsulta = horaConsulta;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
    
    public String escribirCita(){
        String cita=this.codCita+";"+this.especialidad+";"+this.nombreDoctor+";"+this.paciente+";"+this.fechaConsulta+";"+this.horaConsulta;
        return cita;
    }

}
