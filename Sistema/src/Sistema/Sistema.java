
package Sistema;
import Usuarios.Asistente;
import Pacientes.Preferencial;
import Pacientes.Estandar;
import Usuarios.Paciente;
import Archivos.ManejoArchivo;
import Citas.Doctor;
import java.util.ArrayList;
import java.util.Scanner;
public class Sistema {
    public static void main(String[] args) {
       Scanner sc = new Scanner(System.in);
       String[] datosUser=Sistema.accederSistema();
        switch(datosUser[4]){
            case "E":
              //CODIGO PARA PACIENTE ESTÁNDAR
              System.out.print("Detectado paciente estándar\nIngrese su número de cuenta:");
              String nCuentaB = sc.nextLine();
              Paciente paciente = new Estandar(datosUser[0],datosUser[1],datosUser[2],datosUser[3],datosUser[4],nCuentaB);
              boolean retornar=false;
              while(retornar==false){
              switch(Paciente.menu()){
                  case 1:
                      switch(Paciente.menuEspecialidades()){
                          case 1:
                              retornar=paciente.solicitarCita("Pediatría", paciente);
                              break;
                          case 2:
                              retornar=paciente.solicitarCita("Ginecología", paciente);
                              break;
                          case 3:
                              retornar=paciente.solicitarCita("Medicina General", paciente);
                              break;
                      }
                      break;
                  case 2:
                      System.out.println("Ingrese el código de su cita");
                      String codigoCita= sc.nextLine();
                      paciente.consultarCita(codigoCita);
                      break;
                  case 3:
                      System.out.println("Ingrese el código de la cita a cancelar");
                      String codCita= sc.nextLine();
                      paciente.cancelarCita(codCita);
                      break;
              }               
            }
            case "P":
                //CODIGO PARA PACIENTE PREFERENCIAL
                System.out.println("Detectado paciente preferencial");
                Preferencial pac= new Preferencial(datosUser[0],datosUser[1],datosUser[2],datosUser[3],datosUser[4],"0",0,0.5,0.0,"",' ',"","");
                boolean retornar2=false;
                while(retornar2==false){
                    switch(Paciente.menu()){
                        case 1:
                            switch(Paciente.menuEspecialidades()){
                              case 1:
                                  retornar2=pac.solicitarCita("Pediatría", pac);
                                  break;
                              case 2:
                                  retornar2=pac.solicitarCita("Ginecología", pac);
                                  break;
                              case 3:
                                  retornar2=pac.solicitarCita("Medicina General", pac);
                                  break;
                            }
                            if(retornar2==true)
                                pac.setPuntos(pac.getPuntos()+100); 
                            System.out.print("Desea agendar otra cita?(S/N): ");
                            String volver=sc.nextLine();
                            if(volver.equalsIgnoreCase("s"))
                                retornar2=false;
                            break;
                        case 2:
                          //Codigo para consultar cita
                          System.out.println("Ingrese el código de su cita");
                          String codCita= sc.nextLine();
                          pac.consultarCita(codCita);
                          break;
                        case 3:
                            System.out.println("Ingrese el código de la cita a cancelar");
                            String codCita2= sc.nextLine();
                            pac.cancelarCita(codCita2);
                            break;
                    }
                }
                case "A":
                System.out.println("Detectado asistente");
                 switch(Asistente.menuAs()){
                     case 1:
                        //codigo para llenar el perfil del paciente
                        System.out.println("/**********LLENAR PERFIL PACIENTE**********/");
                        System.out.println("Ingrese el usuario: ");
                        String user= sc.nextLine();
                        Asistente ast= new Asistente(datosUser[0],datosUser[1],datosUser[2],datosUser[3],datosUser[4]);
                        ast.llenarPerfilPaciente(user);
                        break;
                     case 4:
//                         codigo para consultar pacientes con cita
//                         System.out.println("/**********PACIENTES CON CITAS AGENDADAS**********/");
//                         ArrayList<Paciente> pacientes= new ArrayList<>();
//                         ArrayList<String> archivo = ManejoArchivo.LeeFichero("citas_medicas.txt");
//                        for (int j = 1; j < archivo.size(); j++) {
//                            String[] linea = archivo.get(j).split(";");
//                            pacientes.add();
//                            
//                        }
                         
                        
                 }
              break;

          }
    }
    
    public static String[] accederSistema(){
        Scanner sc= new Scanner(System.in);
        boolean op=true;
        //ABRIR EL ARCHIVO AQUI
        ArrayList<String> archivo=ManejoArchivo.LeeFichero("usuarios.txt");//Recibe como parámetro el nombre del archivo a abrir 
        System.out.println("\tBIENVENIDO AL SISTEMA");   
        String tipo="";
        String nombre="";
        String apellido="";
        String usuario="";
        String contraseña="";
        while(op!=false){
            System.out.print("Usuario: ");
            usuario=sc.nextLine();
            System.out.print("Contraseña: ");
            contraseña=sc.nextLine();
            //RECUPERAR USUARIO Y CONTRASEÑA DE CADA LINEA EXCEPTO LA PRIMERA
            //VALIDAR EL INGRESO CON LOS DATOS RECUPERADOS
            for(int i=1;i<archivo.size();i++){
                String[] linea = archivo.get(i).split(";");
                if(usuario.equals(linea[2])&&contraseña.equals(linea[3])){
                    System.out.println("Login exitoso");
                    nombre=linea[0];
                    apellido=linea[1];
                    tipo=linea[4];
                    op=false;
                    break;
                }
            }
        }
        String[] datosUser = {nombre,apellido,usuario,contraseña,tipo};
        return datosUser;
    }
    //METODO QUE BUSCA DOCTORES SEGUN LA ESPECIALIDAD INGRESADA
    public static ArrayList<Doctor> buscarDoctores(String especialidad){
         ArrayList<String> infoDoctores=ManejoArchivo.LeeFichero("doctores.txt");//Lee el archivo de doctores y lo guarda como un array
         ArrayList<Doctor> doctores = new ArrayList<>();
         String nombreDoc="";
         String[] horariosDoc;
         for(int i=1;i<infoDoctores.size();i++){
             String[] linea = infoDoctores.get(i).split(";");
             if(especialidad.equalsIgnoreCase(linea[0])){
                nombreDoc=linea[1];
                horariosDoc=linea[2].split(",");
                doctores.add(new Doctor(nombreDoc,horariosDoc,linea[0]));
             }
         }
         return doctores;
         
    }
}