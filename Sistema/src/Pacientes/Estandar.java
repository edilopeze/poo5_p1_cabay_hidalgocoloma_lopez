/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Pacientes;

import Usuarios.Paciente;
import Archivos.ManejoArchivo;
import Citas.Doctor;
import Citas.Cita;
import Sistema.Sistema;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author richy
 */
public class Estandar extends Paciente{
    protected String nCuentaB;
    protected boolean cita;
    public Estandar(String nombre, String apellido, String usuario, String contraseña, String tipo,String cedula,int edad,double peso,double estatura,String fechaNac,char genero, String nCuentaB){
        super(nombre,apellido,usuario,contraseña,tipo,cedula,edad,peso,estatura,fechaNac,genero);
        this.nCuentaB=nCuentaB;
        this.cita=false;
    }
    
    public Estandar(String nombre, String apellido, String usuario, String contraseña, String tipo,String nCuentaB){
    super(nombre,apellido,usuario,contraseña,tipo,"0",0,0.0,0.0,"",'0');
    this.nCuentaB=nCuentaB;
    }

    public boolean getCita() {
        return cita;
    }

    public void setCita(boolean cita) {
        this.cita = cita;
    }

    public String getnCuentaB() {
        return nCuentaB;
    }

    public void setnCuentaB(String nCuentaB) {
        this.nCuentaB = nCuentaB;
    }
    //METODOS SOBREESCRITOS
    @Override
    public boolean solicitarCita(String especialidad, Paciente paciente){
        Random r = new Random();
        boolean retornar=false;
        Scanner sc = new Scanner(System.in);
        ArrayList<Doctor> datosEspecialidad=Sistema.buscarDoctores(especialidad);
        System.out.println("\tDOCTORES DISPONIBLES");
        int indice=1;
        for(Doctor i:datosEspecialidad){
            System.out.println(indice+". "+i.getNombre());
            indice++;
        }
        System.out.print("Elija una opción: ");
        int opDoctor1=sc.nextInt()-1;
        Doctor doctor =datosEspecialidad.get(opDoctor1);
        doctor.mostrarHorarios();
        System.out.print("Elija una opción: ");
        int opConsulta1=sc.nextInt();
        sc.nextLine();
        System.out.print("Desea solicitar la cita en el\nhorario establecido? (S/N):");
        String confirmar=sc.nextLine();
        if(confirmar.equalsIgnoreCase("S")){
            //LLENAR EL DOCUMENTO DE LA CITA
            String codigo = String.valueOf(r.nextInt(999)+2000);
            Cita citaNueva=new Cita(codigo,paciente.getNombre()+" "+paciente.getApellido(),doctor.getNombre(),doctor.getHorarios()[0],doctor.getHorarios()[opConsulta1],doctor.getEspecialidad());
            ManejoArchivo.EscribirArchivo("citas_medicas.txt",citaNueva.escribirCita());
            System.out.println("Su cita se ha agendado con el código: "+citaNueva.getCodigo());
            retornar=true;
        }else{
            System.out.println("Volviendo al menú principal...");

        }
        return retornar;
    }
    
    @Override
    public void consultarCita(String codCita){
        ArrayList<String> file= ManejoArchivo.LeeFichero("citas_medicas.txt");
        System.out.println("/***********CITA PENDIENTE***********/");
        boolean hayCita=false;
        for(int i=1;i<file.size();i++){
            String[] linea= file.get(i).split(",");
            if(codCita.equals(linea[0])){
                System.out.println("ESPECIALIDAD: "+linea[1]);
                System.out.println("DOCTOR: "+linea[2]);
                System.out.println("FECHA: "+linea[4]);
                System.out.println("HORA: "+linea[5]);
                System.out.println("");
                System.out.println("Como usted es un paciente estándar,");
                System.out.println("se han debitado "+25.00+" de su cuenta.");
                hayCita=true;
            }
        }
        if(hayCita==false)
            System.out.println("No existe cita con el código ingresado");
        else
            System.out.println("");
        
    }
    
    @Override
    public void cancelarCita(String codCita){
            ManejoArchivo.EscribirArchivo("solicitudCancelacion.txt",(codCita+","+this.getUsuario()));
            System.out.println("La solicitud ha sido enviada, cuando el asistente lo revise,"
                    + "cancelará su cita.");
        
    }
}
